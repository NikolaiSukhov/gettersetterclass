class Employee {
    constructor(name, age, salary) {
      this._name = name;
      this._age = age;
      this._salary = salary;
    }
  
    get name() {
      return this._name;
    }
    set name(newName) {
      this._name = newName;
    }
  
    get age() {
      return this._age;
    }
    set age(newAge) {
      this._age = newAge;
    }
  
    get salary() {
      return this._salary;
    }
    set salary(newSalary) {
      this._salary = newSalary;
    }
  }
  
  class Programer extends Employee {
    constructor(name, age, salary, language) {
      super(name, age, salary);
      this._language = language;
    }
  
    get salary() {
      return super.salary * 3;
    }
  
    get language() {
      return this._language;
    }
  }
  
  const user = new Employee('sarah' ,19, 1500);
  const prog = new Programer('max', 24 , 4500, "JS");
  const dev = new Programer('alex', 19 , 6500, 'Python');
  console.log(user, prog, dev);
  